import numpy as np
import matplotlib.pyplot as plt
import math

print """
To do this problem I have split up the given formula into three parts because we have one part that requires the point before the one we are looking at, a part that requires the point we are looking at and a part that requires the point 
following the point we are looking at. At the end we take the three functions 
and subtract f1 - f2 - f3 because this is how the taylor expansion works out. 
"""

L = np.array([2.3, 3.4, 4.6, 5.7, 6.9, 8.0, 9.1, 10.3, 11.5, 12.6])

N = np.array([2676, 773, 354, 200, 110, 65, 41, 27, 13, 6])

H = np.array([1.2, 1.1, 1.2, 1.1, 1.2, 1.1, 1.1, 1.2, 1.2, 1.1]) 

h = np.array([1.1, 1.2, 1.1, 1.2, 1.1, 1.1, 1.2, 1.2, 1.1, 1.2])

N[-2] = 8038
N[-1] = 2676
N[0] = 773
N[1] = 354
N[2] = 200
N[3] = 110
N[4] = 65
N[5] = 41
N[6] = 27
N[7] = 13
N[8] = 6
N[9] = 2

H[-2] = 1.2
H[-1] = 1.1
H[0] = 1.2
H[1] = 1.1
H[2] = 1.2
H[3] = 1.1
H[4] = 1.1
H[5] = 1.2
H[6] = 1.2
H[7] = 1.1

h[-2] = 1.1
h[-1] = 1.2
h[0] = 1.1
h[1] = 1.2
h[2] = 1.1
h[3] = 1.1
h[4] = 1.2
h[5] = 1.2
h[6] = 1.1
h[7] = 1.2

def f1(x):
    if x == -1 or 0 or 1 or 2 or 3 or 4 or 5 or 6 or 7 or 8 :
        return (N[x + 1] * H[x - 1]/(h[x - 1] * (H[x - 1] + h[x - 1])))
    else:
        return 0

def f2(x):
    if x == 0 or 1 or 2 or 3 or 4 or 5 or 6 or 7 or 8:
        return (N[x] * (H[x - 1] - h[x - 1]) / (H[x - 1] * h[x - 1]))
    else:
        return 0    

def f3(x):
    if x == 1 or 2 or 3 or 4 or 5 or 6 or 7 or 8:
        return (N[x - 1] * h[x - 1] / (H[x - 1] * (H[x - 1] + h[x - 1])))
    else:
        return 0

print """
The following numbers are the values for log(f1 - f2 - f3) = log(dN/dL) for the points of the given model where the log is in base 10 so that my numbers will 
fit with the plot of observed data which is also in log base 10 of dN/dL
"""
# I had to omit f1(-1) - f2(-1) - f3(-1) and f1(0) - f2(0) - f3(0) because in my
# fucntions above, the values that came out were missing the factor N[x], or 
# N[x - 1] and I am not sure why but the value could not be changed even when
# a number such as 2676 was entered instead of N[x] or similar piece. 

# Each of the following are multiplied by -1 because the values I got were 
# negative which just means I switched H and h, but the values turn out to be
# the same so I can take the absolute value or just multiply by -1 to make them
# positive for my plot.

print math.log10((f1(1) - f2(1) - f3(1)) * -1)
print math.log10((f1(2) - f2(2) - f3(3)) * -1)
print math.log10((f1(3) - f2(3) - f3(3)) * -1)
print math.log10((f1(4) - f2(4) - f3(4)) * -1)
print math.log10((f1(5) - f2(5) - f3(5)) * -1)
print math.log10((f1(6) - f2(6) - f3(6)) * -1)
print math.log10((f1(7) - f2(7) - f3(7)) * -1)
print math.log10((f1(8) - f2(8) - f3(8)) * -1)

print "I also need the log base 10 of the luminosities which are:"
print math.log10(4.6)
print math.log10(5.7)
print math.log10(6.9)
print math.log10(8.0)
print math.log10(9.1)
print math.log10(10.3)
print math.log10(11.5)
print math.log10(12.6)

data = np.loadtxt('ncounts_850.dat')

dNdL = data[:,1]
Luminosity = data[:,0]

x = range(0,3)
y = range(-5,10)
fig = plt.figure()
ax1 =fig.add_subplot(111)

ax1.scatter([math.log10(4.6),  math.log10(5.7), math.log10(6.9), math.log10(8.0), math.log10(9.1), math.log10(10.3), math.log10(11.5), math.log10(12.6)], [math.log10((f1(1) - f2(1) - f3(1)) * -1), math.log10((f1(2) - f2(2) - f3(3)) * -1), math.log10((f1(3) - f2(3) - f3(3)) * -1), math.log10((f1(4) - f2(4) - f3(4)) * -1), math.log10((f1(5) - f2(5) - f3(5)) * -1), math.log10((f1(6) - f2(6) - f3(6)) * -1), math.log10((f1(7) - f2(7) - f3(7)) * -1), math.log10((f1(8) - f2(8) - f3(8)) * -1)], s=75, c='b', marker="s", label='model')
ax1.scatter(Luminosity, dNdL, s=20, c='r', marker="o", label='observed')
plt.legend(loc='upper left');
plt.show()

print """
We can see from the graphed model that although it appears we have an outlier, 
the model seems to fit the data reasonably well.
"""


