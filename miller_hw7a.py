import math
import numpy as np

def pi(n):
    hit = 0.
    for i in range (0, n):
        x = np.random.random()
        y = np.random.random()
        if math.sqrt((x**2.) + (y**2.)) <= 1.:
            hit += 1.
    return ((4. * hit) / n)

print "The following numbers use 100, 1000, 10000 and 10000000 iterations in ascending order"
print pi(100)
print pi(1000)
print pi(10000)
print pi(10000000)
print "Note: through running my code multiple times, I have found that using 10000000 iterations returns a precision of pi to 3.141 about 75% of the time."
