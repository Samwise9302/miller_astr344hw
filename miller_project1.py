import math
import numpy as np
import random

time = 0
xposition = []
yposition = []
infected = []

for i in range(100):
    xposition.append(random.randrange(100))

for i in range(100):
    yposition.append(random.randrange(100))

for i in range(100):
    infected.append(False)

def movement(a, b):
    for i in range(100):
        if a[i] < 0:
            a[i] = 1
        else:
            a[i] = a[i] + random.randrange(-3, 3, 1)

    for k in range(100):
        if b[k] < 0:
            b[k] = 1
        else:
            b[k] = b[k] + random.randrange(-3, 3, 1)

infected[0] = True

while all(infected) == False:
    movement(xposition, yposition)
    for i in range(100):
        if xposition[0] == xposition[i] and yposition[0] == yposition[i]:
            probability1 = random.randrange(100)
            if probability1 < 75:
                infected[i] = True
        if infected[i] == True:
            for j in range(100):
                if xposition[i] == xposition[j] and yposition[i] == yposition[j]:
                    probability = random.randrange(100)
                    if probability < 75:
                        infected[j] = True
    time += 1


print time

"""Since a person who is vaccinated can neither be infected nor infect someone else, they essentially have no need to exist for our model. So I will subtract the number of people vaccinated from the total number of people in the same sized room"""

def movement1(p, q, people):
    for i in range(people):
        if p[i] < 0:
            p[i] = 1
        else:
            p[i] = p[i] + random.randrange(-3, 3, 1)

    for k in range(people):
        if q[k] < 0:
            q[k] = 1
        else:
            q[k] = q[k] + random.randrange(-3, 3, 1)
    

def spread(people):
    time = 0
    infected1 = []
    xposition1 = []
    yposition1 = []

    for i in range(people):
        xposition1.append(random.randrange(100))

    for i in range(people):
        yposition1.append(random.randrange(100))

    for i in range(people):
        infected1.append(False)

    infected1[0] = True

    while all(infected1) == False:
        movement1(xposition1, yposition1, people)
        for i in range(people):
            if xposition1[0] == xposition1[i] and yposition1[0] == yposition1[i]:
                probability1 = random.randrange(100)
                if probability1 < 75:
                    infected1[i] = True
            if infected1[i] == True:
                for j in range(people):
                    if xposition1[i] == xposition1[j] and yposition1[i] == yposition1[j]:
                        probability = random.randrange(100)
                        if probability < 75:
                            infected1[j] = True
        time += 1
    return time

print spread(90)
print spread(80)
print spread(70)
print spread(60)
print spread(50)
print spread(40)
print spread(30)
print spread(20)
print spread(10)


def bufferspread(people, prob):
    time = 0
    infected1 = []
    xposition1 = []
    yposition1 = []

    for i in range(people):
        xposition1.append(random.randrange(100))

    for i in range(people):
        yposition1.append(random.randrange(100))

    for i in range(people):
        infected1.append(False)

    infected1[0] = True

    while all(infected1) == False:
        movement1(xposition1, yposition1, people)
        for i in range(people):
            if xposition1[0] == xposition1[i] and yposition1[0] == yposition1[i]:
                probability1 = random.randrange(100)
                if probability1 < prob:
                    infected1[i] = True
            if infected1[i] == True:
                for j in range(people):
                    if xposition1[i] == xposition1[j] and yposition1[i] == yposition1[j]:
                        probability = random.randrange(100)
                        if probability < prob:
                            infected1[j] = True
        time += 1
    return time

print bufferspread(100, 60)
print bufferspread(100, 50)
print bufferspread(100, 40)
print bufferspread(100, 30)
print bufferspread(100, 20)
