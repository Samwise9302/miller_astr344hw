import numpy as np
import matplotlib.pyplot as plt
import pdb
import math


C = 3000
M = .3
V = .7

def r(z):
    return (C /(math.sqrt(((M * ((1 + z) ** 3) + V)))))

def T(f, p, q, n):
    h = float(q - p) / n
    s = 0.0
    s += f(p) / 2.0
    for i in range(1, n):
        s += f(p + (i * h))
    s += f(q) / 2.0
    return s * h

def Da(f, p, q, n):
    return T(f, p, q, n) / (1 + q)

y = []
x = []

for i in range(1, 50):
    x.append((5. / 50) * i)

for i in x:
    y.append(Da(r, 0, i, 50))

y0 = []
y1 = []
y2 = []
y3 = []
y4 = []
y5 = []

x0 = []
x1 = []
x2 = []
x3 = []
x4 = []
x5 = []

for i in range(0, 50):
    x0.append(0)

for i in range(0, 50):
    x1.append((1. / 50) * i)

for i in range(0, 50):
    x2.append((2. / 50) * i)

for i in range(0, 50):
    x3.append((3. / 50) * i)

for i in range(0, 50):
    x4.append((4. / 50) * i)

for i in range(0, 50):
    x5.append((5. / 50) * i)

for i in range(0, 50):
    y0.append(r((0) / 50))

for i in range(0, 50):
    y1.append(r((1. * i) / 50))

for i in range(0, 50):
    y2.append(r((2. * i) / 50))

for i in range(0, 50):
    y3.append(r((3. * i) / 50))

for i in range(0, 50):
    y4.append(r((4. * i) / 50))

for i in range(0, 50):
    y5.append(r((5. * i) / 50))

tda0 = np.trapz(y0, x =  x0) / 1
tda1 = np.trapz(y1, x =  x1) / 2
tda2 = np.trapz(y2, x =  x2) / 3
tda3 = np.trapz(y3, x =  x3) / 4
tda4 = np.trapz(y4, x =  x4) / 5
tda5 = np.trapz(y5, x =  x5) / 6

tday = [tda0, tda1, tda2, tda3, tda4, tda5]
tdax = [0, 1, 2, 3, 4, 5]

#x = range(0, 5)
#y = range(0, 1300)
fig = plt.figure()
ax1 = fig.add_subplot(111)
ax1.scatter(x, y, s = 150, c = 'g', marker = "o", label = 'Sam_plot')
ax1.scatter(tdax, tday, s = 150, c = 'r', marker = "s", label = 'np.trapz')

plt.legend(loc = 'upper left');
plt.show()

print """
Clearly my answer is almost identical to the canned routine as the points from
the np.trapz function seem to fit perfectly along my fit.
"""
