import numpy as np

x = np.float32(1.)
y = np.float32(3.)

# single precision

def recurrence_x(n):
    if n == 0:
        return 1
    else: 
        return ((x/y) ** n + recurrence_x(n-1))

print recurrence_x(0)
print recurrence_x(1)
print recurrence_x(2)
print recurrence_x(3)
print recurrence_x(4)
print recurrence_x(5)

# Now we will do the double precision so we can calculate errors

p = np.float64(1.)
q = np.float64(3.)

def x_dp(n):
    if n == 0:
        return 1
    else: 
        return ((p/q) ** n + x_dp(n-1))

print x_dp(0)
print x_dp(1)
print x_dp(2)
print x_dp(3)
print x_dp(4)
print x_dp(5)

# Now we can calculate errors using double precision as real and single 
# precision as x bar.

abs_err_5 = x_dp(5) - recurrence_x(5)
rel_err_5 = (abs_err_5) / (x_dp(5))

print "absolute error for n = 5 is",  abs_err_5
print "relative error for n = 5 is",  rel_err_5

abs_err_20 = x_dp(20) - recurrence_x(20)
rel_err_20 = (abs_err_20) / (x_dp(20))

print "absolute error for n = 20 is", abs_err_20
print "relative error for n = 20 is", rel_err_20

# Now we go through the process again using the different formula

a = np.float32(4.)

def xnew_sp(n):
    if n == 0:
        return 1
    else:
        return (a ** n + xnew_sp(n-1))

b = np.float64(4.)

def xnew_dp(n):
    if n == 0:
        return 1
    else:
        return (b ** n + xnew_dp(n-1))

# The values for the single and double precision are the same for some reason.
# I use g below to try to make single precision actually give a single
# precision value.

g = np.float32(xnew_sp(20))

print "single precision value n = 20 is", g
print "double precision value n = 20 is", xnew_dp(20)

new_abs_err_20 = xnew_dp(20) - g
new_rel_err_20 = (new_abs_err_20) / (xnew_dp(20))

print "The absolute error for the new function when n = 20 is", new_abs_err_20
print "The relative error for the new function when n = 20 is", new_rel_err_20

print """
This calculation is stable because of how small the relative error is. What this is saying is that compared to the actual value of the number we are calculating the error is of an order of magnitude that is relatively insignificant. As a result, relative error should be used to measure accuracy and stability because absolute error merely gives you a number with no regard as to how significant that number is with regard to the value you are calculating.
"""

