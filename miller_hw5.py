import math
import numpy as np

def f(x):
    return ((x ** 2) + x - 6)

tolerance = .005
endpoint1 = 0.
endpoint2 = 5.

def bracket_checkf(endpoint1, endpoint2):
    if (f(endpoint1) * f(endpoint2)) > 0:
        return "No root contained within bounds"
    else:
        return loop(endpoint1, endpoint2)


def loop(endpoint1, endpoint2):
    count = 0
    root = False
    while(count < 1000 and root == False):
        mid = ((endpoint1 + endpoint2) / 2.)
        count = count + 1
        if (f(mid) * f(endpoint1)) > 0.:
            endpoint1 = mid
        if (f(mid) * f(endpoint2)) > 0.:
            endpoint2 = mid
        if math.fabs(f(endpoint1)) <= tolerance:
            root = True
            return endpoint1
        if math.fabs(f(endpoint2)) <= tolerance:
            root = True
            return endpoint2

print bracket_checkf(endpoint1, endpoint2)

def g(x):
    return (x ** 3.) - x - 2.

Eg1 = 1
Eg2 = 2

def bracket_checkg(Eg1, Eg2):
    if (g(Eg1) * g(Eg2)) > 0:
        return "No root contained within bounds"
    else:
        return loop(Eg1, Eg2)


def loop(Eg1, Eg2):
    count = 0
    root = False
    while(count < 1000 and root == False):
        mid = ((Eg1 + Eg2) / 2.)
        count = count + 1.
        if (g(mid) * g(Eg1)) > 0.:
            Eg1 = mid
        if (g(mid) * g(Eg2)) > 0.:
            Eg2 = mid
        if math.fabs(g(Eg1)) <= tolerance:
            root = True
            return Eg1
        if math.fabs(g(Eg2)) <= tolerance:
            root = True
            return Eg2

print bracket_checkg(Eg1, Eg2)

def y(x):
    return np.sin(x)

Ey1 = 2
Ey2 = 4

def bracket_checky(Ey1, Ey2):
    if (y(Ey1) * y(Ey2)) > 0:
        return "No root contained within bounds"
    else:
        return loop(Ey1, Ey2)

def loop(Ey1, Ey2):
    count = 0
    root = False
    while(count < 1000 and root == False):
        mid = ((Ey1 + Ey2) / 2.)
        count = count + 1.
        if (y(mid) * y(Ey1)) > 0.:
            Ey1 = mid
        if (y(mid) * y(Ey2)) > 0.:
            Ey2 = mid
        if math.fabs(y(Ey1)) <= tolerance:
            root = True
            return Ey1
        if math.fabs(y(Ey2)) <= tolerance:
            root = True
            return Ey2

print bracket_checky(Ey1, Ey2)
