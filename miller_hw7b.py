import math
import numpy as np
import random


def bday(n, people):
    hits = 0
    for j in range(0, n): # This creates n empty lists
        l = []
        
        for i in range(people): # This appends the number of 'people'random  integers between 0 and 365 to each of the n empty lists.
            i = random.randrange(365)
            l.append(i)
        
        match = False # This checks each of the lists for identical birthdays and adds one to our counter, called 'hits' every time there is a match.
        for k in l:
            if l.count(k) > 1:
                match = True
        if match == True:
            hits += 1.
    return (hits / n) # probability = hits / n
    



print """
The following values are calculated using 10000 iterations and a room with 50, 30, 25, 23 and 22 people in descending order. this shows that as we approach a room of 23 people the probability of two of them having the same birthday approaches 50 percent with the probability dropping just below 50 percent when the number of people drops below 23.
"""
print bday(10000, 50)
print bday(10000, 30)
print bday(10000, 25)
print bday(10000, 23)
print bday(10000, 22)
 
    
