import math
import numpy as np
from datetime import datetime

tolerance = .005
h = .00001
y = 1.1

"""I will start by defining the three functions for which I will find the root 
using the N-R method. Next I will define their derivative which is required for calculating the root in the N-R method. Finally I will use the N-R method which will calculate the root. I will also include the amount of time it takes for the N-R method to calculate the root which I will then compare to the time it takes for the bisection method to calculate the same root."""

def g(x):
    return (x ** 3.) - x - 2.

def f(x):
    return np.sin(x)

def y(x):
    return (x ** 2) + x - 6

#The above three function are the functions I will be finding the root for

def primeg(x):
    return (g(x + h) - g(x - h)) / (2. * h)

def primey(x):
    return (y(x + h) - y(x - h)) / (2. * h)

def primef(x):
    return (f(x + h) - f(x - h)) / (2. * h)

#The above three functions are the derivatives at a point calculated numerically


#The following three functions will actually calculate the root using the N-R 
#method. I have also included the datetime.now so that I can calculate the time 
#it takes for the N-R method to actually find the root.
tnrg1 = datetime.now()

def rootg(x):
    root = False
    while(root == False):
        if math.fabs(g(x)) <= tolerance:
            root = True
            return x
        if math.fabs(g(x)) > tolerance:
            x = x - ((g(x)) / (primeg(x)))
    return x

tnrg2 = datetime.now()

tnrg = tnrg2 - tnrg1

tnrf1 = datetime.now()

def rootf(x):
    root = False
    while(root == False):
        if math.fabs(f(x)) <= tolerance:
            root = True
            return x
        if math.fabs(f(x)) > tolerance:
            x = x - ((f(x)) / (primef(x)))
    return x

tnrf2 = datetime.now()

tnrf = tnrf2 - tnrf1

tnry1 = datetime.now()

def rooty(x):
    root = False
    while(root == False):
        if math.fabs(y(x)) <= tolerance:
            root = True
            return x
        if math.fabs(y(x)) > tolerance:
            x = x - ((y(x)) / (primey(x)))
    return x

tnry2 = datetime.now()

tnry = tnry2 - tnry1

print "The root of g(x) is:"
print rootg(1.1)

print "The time it takes for the N-R method to calculate the root is:"
print tnrg

print "The root of f(x) is:"
print rootf(3.13)

print "The time it takes for the N-R method to calculate the root is:"
print tnrf

print "The root of y(x) is:"
print rooty(1.)

print "The time it takes for the N-R method to calculate the root is:"
print tnry

"""Now I am going to insert my bisection method from last week's homework so 
that I can use datetime.now to find out how long it takes the bisection method 
to find the same roots as I just found using the N-R method and compare which 
one takes longer to calculate the root."""

tbig1 = datetime.now()

Eg1 = 1
Eg2 = 2

def bracket_checkg(Eg1, Eg2):
    if (g(Eg1) * g(Eg2)) > 0:
        return "No root contained within bounds"
    else:
        return loop(Eg1, Eg2)


def loop(Eg1, Eg2):
    count = 0
    root = False
    while(count < 1000 and root == False):
        mid = ((Eg1 + Eg2) / 2.)
        count = count + 1.
        if (g(mid) * g(Eg1)) > 0.:
            Eg1 = mid
        if (g(mid) * g(Eg2)) > 0.:
            Eg2 = mid
        if math.fabs(g(Eg1)) <= tolerance:
            root = True
            return Eg1
        if math.fabs(g(Eg2)) <= tolerance:
            root = True
            return Eg2

tbig2 = datetime.now()
tbig = tbig2 - tbig1

tbif1 = datetime.now()

Ef1 = 2.
Ef2 = 4.

def bracket_checkf(Ef1, Ef2):
    if (f(Ef1) * f(Ef2)) > 0:
        return "No root contained within bounds"
    else:
        return loop(Ef1, Ef2)

def loop(Ef1, Ef2):
    count = 0
    root = False
    while(count < 1000 and root == False):
        mid = ((Ef1 + Ef2) / 2.)
        count = count + 1.
        if (f(mid) * f(Ef1)) > 0.:
            Ef1 = mid
        if (f(mid) * f(Ef2)) > 0.:
            Ef2 = mid
        if math.fabs(f(Ef1)) <= tolerance:
            root = True
            return Ef1
        if math.fabs(f(Ef2)) <= tolerance:
            root = True
            return Ef2

tbif2 = datetime.now()
tbif = tbif2 - tbif1


tbiy1 = datetime.now()

endpoint1 = 0.
endpoint2 = 5.

def bracket_checky(endpoint1, endpoint2):
    if (y(endpoint1) * y(endpoint2)) > 0:
        return "No root contained within bounds"
    else:
        return loop(endpoint1, endpoint2)

def loop(endpoint1, endpoint2):
    count = 0
    root = False
    while(count < 1000 and root == False):
        mid = ((endpoint1 + endpoint2) / 2.)
        count = count + 1
        if (y(mid) * y(endpoint1)) > 0.:
            endpoint1 = mid
        if (y(mid) * y(endpoint2)) > 0.:
            endpoint2 = mid
        if math.fabs(y(endpoint1)) <= tolerance:
            root = True
            return endpoint1
        if math.fabs(y(endpoint2)) <= tolerance:
            root = True
            return endpoint2

tbiy2 = datetime.now()
tbiy = tbiy2 - tbiy1

print "The time it takes for the bisection method to calculate the root for g(x) is:"
print tbig
print "The time it takes for the bisection method to calculate the root for f(x) is:"
print tbif
print "The time it takes for the bisection method to calculate the root for y(x)is:"
print tbiy

#This is the end of part one. Everything that follows pertains to part 2.

def Intensity(T):
    h = np.float64(6.6260755 * (10 ** (-27)))
    c = np.float64(2.99792458 * (10 ** 10))
    k = np.float64(1.380658 * (10 ** (-16)))
    B = np.float64(1.25 * (10 ** (-12)))
    v = np.float64(344827586206.89655)
    return (((2. * h * (v ** 3.)) / (c ** 2.)) / (math.exp((h * v) / (k * T)) - 1.)) - B

dx = 1.
tol = (1.0 * (10 ** (-15)))

def primeIntensity(T):
    return (Intensity(T + dx) - Intensity(T - dx)) / (2. * dx)

def rootIntensity(z, x):
    root = False
    while(root == False):
        zx = z(x)
        if math.fabs(zx) <= tol:
            root = True
            return x
        if math.fabs(zx) > tol:
            x = x - ((zx) / ((z(x + dx) - z(x - dx)) / (2. * dx)))
    return x

print "The temperature in Kelvin to the corresponding Luminosity is:"
print rootIntensity(Intensity, 30)
